import csv, sys, copy

# Node Class to hold the network's node and it's adjacent nodes (neighbors)
class Node:

    # Constructor to initialize node object with node name and create a dict to hold its neighbor and cost 
    def __init__(self,key):
        self.key = key
        self.neighbors = {}
        self.distance = {}

    def addNeighbor(self, node, cost):
        self.neighbors[node] = cost
    
    def sendDistanceVector(self):
        for neighbor in self.neighbors.keys():
            if (neighbor.key != self.key):
                neighbor.distance[self.key] = {}
                for name in nodeIndex.values():
                    neighbor.distance[self.key][name] = sys.maxsize # infinity

# Graph Class to hold the network nodes
class Graph:
    # Constructor with number of nodes and node Name to Node Mapping
    def __init__(self):
        self.nodeMap = {}

    # adds node to the graph
    def addNode(self, key):
        self.nodeMap[key] = Node(key)

    # Returns the node by name
    def getNode(self, key):
        if key in self.nodeMap:
            return self.nodeMap[key]
        else:
            return None

    # Adds edges to the graph
    def addEdge(self, source, destination, weight):
        if source not in self.nodeMap:
            self.addNode(source)
        if destination not in self.nodeMap:
            self.addNode(destination)
        self.nodeMap[source].addNeighbor(self.nodeMap[destination], weight)

    # print distance vector in console
    def printDistanceVector(self):
        for name, node in self.nodeMap.items():
            print(f'Distance vector for node {name}: ', ' '.join(str(v) for v in node.distance[node.key].values()))

# main function: this is where the start of the program execution occurs
if __name__ == "__main__":
    
    # read the filename from the command line arguments
    filename = sys.argv[1]
   
    nodeIndex = {}
    graph = Graph()

    # parsing csv file
    with open(filename) as csvFile:

        # read the whole csv file into data
        data = csv.reader(csvFile)

        # loop over each row data
        for ri, row in enumerate(data):

            #loop over each column
            for ci, column in enumerate(row):
                if ci > 0:
                    # initialize the graph node names from the heading and skip 
                    if ri == 0:
                        graph.addNode(column)
                        nodeIndex[ci] = column
                        continue
                    
                    # add edges to the graph
                    cost = int(column)
                    if cost == 9999:
                        cost = sys.maxsize
                    graph.addEdge(row[0], nodeIndex[ci], cost)
    
    # 1. Initialize
    for curNode in nodeIndex.values():
        node = graph.getNode(curNode)
        node.distance[curNode] = {}
        # for all destinations y in N: 
        for neiNode in nodeIndex.values():
            yNode = graph.getNode(neiNode)
            node.distance[curNode][neiNode] = node.neighbors[yNode]

        # for each neighbor w
        for neighbor in node.neighbors.keys():
            if (neighbor != node):
                neighbor.distance[curNode] = {}
                for name in nodeIndex.values():
                    neighbor.distance[curNode][name] = sys.maxsize # infinity
        
        # send distance vector
        node.sendDistanceVector()

    # 2. Repeat
    while True:
        stopLoop = True
        for xNode in nodeIndex.values():
            xnode = graph.getNode(xNode)
            tmp = copy.deepcopy(xnode)

            for yNode in nodeIndex.values():
                if xNode != yNode:
                    # Dx(y) = minv{c(x,v) + Dv(y)}
                    xnode.distance[xNode][yNode] = min([cost + v.distance[v.key][yNode] for v, cost in xnode.neighbors.items()])

            # checking if the distance vector changed for a node
            isSame = tmp.distance == xnode.distance
            if (not isSame):
                xnode.sendDistanceVector()
            stopLoop = stopLoop and isSame

        # stop loop if all the distance vectors don't change
        if stopLoop:
            break    
    
    graph.printDistanceVector()     
